/**
 Copyright (C) 2021 Softwarehuis

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Layout } from 'antd';
import { Component } from 'react';
import './App.css';
import { MyContent } from './components/Content';
import { MyFooter } from './components/Footer';
import { MyHeader } from './components/Header';
import { IntlProps } from './Core';
import { injectIntl } from 'react-intl';
import { getMunicipalities, getPostalCodes } from './services/service';

export class App extends Component<IntlProps> {
  constructor(props: any) {
    super(props)
    getMunicipalities("nl").then((data: any)=> {
      console.log(data)
    })
    getPostalCodes("nl", "assen").then((data: any)=> {
      console.log(data.postal_codes)
    })
  }
  
  render() {
    
    return (
      <Layout className="layout">
        <MyHeader />
        <MyContent intl={this.props.intl}/>
        <MyFooter />
      </Layout>
    )
  }
}

export default injectIntl(App);