/**
 Copyright (C) 2021 Softwarehuis

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import './Steps.css';
import { Button, Form, Input, message, Modal, Space, Steps } from 'antd';
import { Component } from 'react';
import { Form1 } from '../forms/Form.1';
import { Form2 } from '../forms/Form.2';
import { Form3 } from '../forms/Form.3';
import { ReportsTable } from './ReportsTable';
import { IntlProps, Questionaire, Report } from '../Core';
import { defaultQuestionaire } from '../Core';
import { FormattedMessage } from 'react-intl';
import {
    ArrowLeftOutlined,
    ArrowRightOutlined,
} from '@ant-design/icons';

const { Step } = Steps;

export class MySteps extends Component<IntlProps> {
    /**
     * Stepper to walk through various steps in reporting. Contains the store for the whole questionaire
     * and holds an indication in state about the active step
     */
    readonly state: {
        current: number,
        questionaire: Questionaire,
        verified: boolean,
        verifying: boolean,
        modalVisible: boolean,
        code: string
    } = {
            current: 0,
            questionaire: defaultQuestionaire,
            verified: false,
            modalVisible: false,
            verifying: false,
            code: ""

        }
        

    onChangeWho = (e: any) => {
        /**
         * Reporter indicated to for who the report is (number {self: 0, others: 1})
         */
        this.setState({
            questionaire: {
                ...this.state.questionaire,
                reporter: { ...this.state.questionaire.reporter, who: e.target.value }
            }
        })
    }

    onChangeProfessional = (e: any) => {
        /**
         * Reporter indicated to be a professional or not (boolean)
         */
        this.setState({
            questionaire: {
                ...this.state.questionaire,
                reporter: { ...this.state.questionaire.reporter, professional: e.target.value }
            }
        })
    }

    onChangeProfession = (e: any) => {
        /**
         * Reporter entered information about profession (plain text)
         */
        this.setState({
            questionaire: {
                ...this.state.questionaire,
                reporter: { ...this.state.questionaire.reporter, profession: e.target.value }
            }
        })
    }


    onChangeExpert = (e: any) => {
        /**
         * Reporter entered information about profession (plain text)
         */
        this.setState({
            questionaire: {
                ...this.state.questionaire,
                reporter: { ...this.state.questionaire.reporter, expert: e.target.value }
            }
        })
    }

    onChangeCode = (e: any) => {
        // This function requires the onChangeReport to be wrapped in the callback
        this.setState({
            code: e.target.value
        })
    }

    onChangeReport = (report: Report, callback: (key: number) => {}) => {
        /**
         * Receives individuale report changes and either creates, updates or adds a report to the
         * reports in the questionaire that is stored in the state.
         * Returns the key of the object created/updated/added via a callback to the component
         * that triggered the change. That component should then update the key if necessary.
         */
        report.autopsy = report.deceased ? report.autopsy : report.autopsy = false
        let reports: Report[] = this.state.questionaire.reports
        if (reports.length === 0) {
            console.debug("Create")
            report.key = 0
            reports = [report]

        } else {
            let activeReport: Report | undefined = reports.find((hit: any) => {
                return hit.key === report.key
            })
            if (activeReport) {
                console.debug("Update")
                reports = reports.map((hit: Report) => {
                    return hit.key === report.key ? report : hit
                })
            } else {
                console.debug("Add")
                report.key = reports.length - 1
                reports.push(report)
            }
        }
        this.setState({
            questionaire: {
                ...this.state.questionaire,
                reports: reports
            }
        }, () => {
            console.log(JSON.stringify(report))
            callback(report.key)
        })
    }

    next = () => {
        /**
         * Next button pushed by a user. Steps go forward 1 step
         */
        this.setState({ current: this.state.current + 1 })
    }

    prev = () => {
        /**
         * Next button pushed by a user. Steps go back 1 step
         */
        this.setState({ current: this.state.current - 1 })
    }

    verify = () => {
        /**
         * Next button pushed by a user. Steps go back 1 step
         */
        this.setState({ modalVisible: true })
    }

    handleModalOk = () => {
        console.log(this.state.code)
        this.setState({ verifying: true })
        setTimeout(() => {
            if (this.state.code === "000000" || this.state.code.length < 6) {
                // Do something
            } else {
                this.setState({ modalVisible: false })
                this.setState({ verifying: false })
                this.setState({ verified: true})
                this.next()
            }
        }, 2000)
    }

    handleModalCancel = () => {
        this.setState({ modalVisible: false })
    }

    render() {
        const { questionaire } = this.state
        const { intl } = this.props
        const steps: any[] = [

            {
                title: <FormattedMessage
                    id="steps.reporter"
                    description="Welcome"
                    defaultMessage="Welcome" />,
                content: <Form1
                    intl={intl}
                    reporter={questionaire.reporter}
                    onChangeWho={this.onChangeWho}
                    onChangeProfessional={this.onChangeProfessional}
                    onChangeProfession={this.onChangeProfession}
                    onChangeExpert={this.onChangeExpert} />,
            },
            {
                title: <FormattedMessage
                    id="steps.verification"
                    description="Verification"
                    defaultMessage="Verification" />,
                content: <>
                    <Form2 intl={intl} reporter={questionaire.reporter}
                        onFinish={this.verify}
                    />
                </>,
            },
            {
                title: <FormattedMessage
                    id="steps.report"
                    description="Report"
                    defaultMessage="Report" />,
                content: <>
                    <Form3 intl={intl} reporter={questionaire.reporter}
                        onChangeReport={this.onChangeReport} />
                </>,
            },
            {
                title: <FormattedMessage
                    id="steps.finalize"
                    description="Finalize"
                    defaultMessage="Finalize" />,
                content: questionaire.reporter.who === 1 &&
                    <ReportsTable reports={questionaire.reports} />
                ,
            },
        ]
        return (
            <>
                <Steps current={this.state.current}>
                    {steps.map(item => (
                        <Step key={item.title} title={item.title} />
                    ))}
                </Steps>
                <div className="steps-content">{steps[this.state.current].content}</div>
                <div className="steps-action">
                    <Space>
                        {this.state.current > 0 && (
                            <Button size="large" shape="round" onClick={() => this.prev()}>
                                <ArrowLeftOutlined />
                                <FormattedMessage
                                    id="steps.previous"
                                    description="Previous"
                                    defaultMessage="Previous" />
                            </Button>
                        )}
                        {!this.state.verified && this.state.current === 1 && (
                            <><Button danger form="VerificationForm" htmlType="submit" size="large" shape="round">
                                <FormattedMessage
                                    id="steps.verify"
                                    description="Verify"
                                    defaultMessage="Verify" />
                            </Button>
                                <Modal
                                    closable={false}
                                    visible={this.state.modalVisible}
                                    title={intl.formatMessage({ id: "modal.verify_title", defaultMessage: "A verification is send to your email. Check your email and enter the code" })}
                                    confirmLoading={this.state.verifying}
                                    onOk={this.handleModalOk}
                                    onCancel={this.handleModalCancel}>
                                    <Form>
                                    <Form.Item
                                        label="Code"
                                        rules={[{ required: true, message: 'Please input a valid code' }]}
      >                                        <Input style={{ width: '30%'}} placeholder="000000" onChange={this.onChangeCode}/>
                                        </Form.Item>
                                    </Form>
                                </Modal>
                            </>
                        )}
                        {this.state.current < steps.length - 1 && (this.state.verified || this.state.current === 0) && (
                            <Button type="primary" size="large" shape="round" onClick={() => this.next()}>
                                <FormattedMessage
                                    id="steps.next"
                                    description="Next"
                                    defaultMessage="Next" />
                                <ArrowRightOutlined />
                            </Button>
                        )}
                        {this.state.current === steps.length - 1 && (
                            <Button type="primary" size="large" shape="round" onClick={() => message.success(<FormattedMessage
                                id="steps.thankyou"
                                description="Thank you"
                                defaultMessage="Thank you" />)}>
                                <FormattedMessage
                                    id="steps.ready"
                                    description="Ready"
                                    defaultMessage="Ready" />
                            </Button>
                        )}
                    </Space>
                </div>
            </>
        )
    }
}