/**
 Copyright (C) 2021 Softwarehuis

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import './Content.css';
import { Content } from 'antd/lib/layout/layout';
import { Component } from 'react';
import { MySteps } from './Steps';
import { IntlProps } from '../Core';


export class MyContent extends Component<IntlProps> {
    render() {
        return (
            <Content className="site-layout-content">
                <MySteps intl={this.props.intl}/>
            </Content>
        )
    }
}