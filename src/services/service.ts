
export const getMunicipalities = async (country: string) => {
    const response = await fetch(
        "https://vaccinatiebijwerkingen.nl/api/v1/" + country + "/municipalities.json"
    )
    const data = await response.json();
    return data
}

export const getPostalCodes = async (country: string, municipality: string) => {
    const response = await fetch(
        "https://vaccinatiebijwerkingen.nl/api/v1/" + country + "/municipality/" + municipality + ".json"
    )
    const data = await response.json();
    return data
}