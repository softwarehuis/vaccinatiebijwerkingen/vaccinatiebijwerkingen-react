/**
 Copyright (C) 2021 Softwarehuis

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import reportWebVitals from './reportWebVitals'
import { ConfigProvider } from 'antd'
import { IntlProvider } from 'react-intl'
import nlNL from 'antd/lib/locale/nl_NL'

function loadLocaleData(locale: string) {
  switch (locale) {
    case 'nl': return import('./lang/nl.json')
    default: return import('./lang/en.json')
  }
}

function Main(props: any) {
  return (
    <IntlProvider locale={navigator.language} defaultLocale="en" messages={props.messages}>
      <App />
    </IntlProvider>
  )
}
async function bootstrapApplication(locale: string) {
  const messages = await loadLocaleData(locale)
  ReactDOM.render(
    <React.StrictMode>
      <ConfigProvider locale={nlNL}>
        <Main locale={locale} messages={messages} />,
      </ConfigProvider>
    </React.StrictMode>,
    document.getElementById('root')
  )
}

bootstrapApplication(navigator.language)
reportWebVitals()
