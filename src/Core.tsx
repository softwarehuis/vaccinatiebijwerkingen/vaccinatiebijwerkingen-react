/**
 Copyright (C) 2021 Softwarehuis

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Select, Space, Tag } from "antd"
import {
    EditOutlined,
    DeleteOutlined,
    CheckOutlined
} from '@ant-design/icons'
import { FormattedMessage, IntlShape } from "react-intl"
import { ReactElement } from "react"

const { Option } = Select

export const reasons: any[] = [
    <Option key="prevent" value="prevent"><FormattedMessage
        id="reason.prevent"
        description="Prevent disease"
        defaultMessage="Prevent disease" /></Option>,
    <Option key="social" value="social"><FormattedMessage
        id="reason.social"
        description="Work/Social/Societal"
        defaultMessage="Work/Social/Societal" /></Option>,
]

export const ages: Age[] = [
    {
        id: "<20",
        title: <FormattedMessage
            id="ages.agegroup1"
            description="younger than 20"
            defaultMessage="younger than 20" />
    },
    {
        id: "20-40",
        title: <FormattedMessage
            id="ages.agegroup2"
            description="between 20 and 40"
            defaultMessage="between 20 and 40" />
    },
    {
        id: "40-65",
        title:

            <FormattedMessage
                id="ages.agegroup3"
                description="between 40 and 65"
                defaultMessage="between 40 and 65" />
    },
    {
        id: "65-80",
        title: <FormattedMessage
            id="ages.agegroup4"
            description="between 65 and 80"
            defaultMessage="between 65 and 80" />
    },
    {
        id: ">80", title: <FormattedMessage
            id="ages.agegroup5"
            description="80 and older"
            defaultMessage="80 and older" />
    },
]

export function setAge(id: string): Age {
    let activeAge: Age | undefined = ages.find((hit: any) => {
        return hit.id === id
    })
    if (activeAge) return activeAge
    return ages[0]

}

export const expert = [
    {
        label: <FormattedMessage
            id="yes"
            description="Yes"
            defaultMessage="Yes" />, value: true
    },
    {
        label: <FormattedMessage
            id="no"
            description="No"
            defaultMessage="No" />, value: false
    },
]

export const deceased = [
    {
        label: <FormattedMessage
            id="yes"
            description="Yes"
            defaultMessage="Yes" />, value: true
    },
    {
        label: <FormattedMessage
            id="no"
            description="No"
            defaultMessage="No" />, value: false
    },
]
export const autopsy = [
    {
        label: <FormattedMessage
            id="yes"
            description="Yes"
            defaultMessage="Yes" />, value: true
    },
    {
        label: <FormattedMessage
            id="no"
            description="No"
            defaultMessage="No" />, value: false
    },
]
export const medical = [
    {
        label: <FormattedMessage
            id="yes"
            description="Yes"
            defaultMessage="Yes" />, value: true
    },
    {
        label: <FormattedMessage
            id="no"
            description="No"
            defaultMessage="No" />, value: false
    },
]

export const defaultQuestionaire: Questionaire = {
    reporter: {
        who: 0, professional: false, profession: "", expert: false
    },
    reports: []
}
export const table: { data: Report[], columns: any } = {
    data: [

    ],
    columns: [
        {
            title: <FormattedMessage
                id="name"
                defaultMessage="Name" />,
            dataIndex: 'name',
            key: 'name',
            width: 100,
            fixed: 'left'
        },
        {
            title: <FormattedMessage
                id="age"
                defaultMessage="Age" />,
            dataIndex: 'age',
            key: 'age',
        },
        {
            title: <FormattedMessage
                id="area"
                defaultMessage="Area" />,
            dataIndex: 'area',
            key: 'area',
        },
        {
            title: <FormattedMessage
                id="deceased"
                defaultMessage="Deceased" />,
            dataIndex: 'deceased',
            key: 'deceased',
            render: (deceased: boolean) => (
                <>
                    {deceased &&
                        <CheckOutlined />
                    }
                </>
            )
        },
        {
            title: <FormattedMessage
                id="autopsy"
                defaultMessage="Autopsy" />,
            dataIndex: 'autopsy',
            key: 'autopsy',
            render: (autopsy: boolean) => (
                <>
                    {autopsy &&
                        <CheckOutlined />
                    }
                </>
            )
        },
        {
            title: <FormattedMessage
                id="reasons.reasons"
                defaultMessage="Reasons" />,
            key: 'reasons',
            dataIndex: 'reasons',
            render: (reasons: string[]) => (
                <>
                    {reasons.map(reason => {
                        return (
                            <Tag key={reason}>
                                {reason}
                            </Tag>
                        )
                    })}
                </>
            )
        }, {
            title: <FormattedMessage
                id="actions"
                defaultMessage="Actions" />,
            fixed: 'right',
            key: 'action',
            render: (text: string, record: { name: string }) => (
                <Space size="middle">
                    <EditOutlined />
                    <DeleteOutlined />
                </Space>
            ),
        },
    ]
}

export interface Report {
    key: number
    name: string
    area: string
    deceased: boolean
    autopsy: boolean
    medical: boolean
    age: any
    reasons: string[]
}

export interface Reporter {
    email?: string
    phone?: string
    professional: boolean
    expert: boolean
    who: number
    profession: string | undefined
}
export interface Questionaire {
    reporter: Reporter
    reports: Report[]
}

export interface QuestionaireProps {
    questionaire: Questionaire
}

export interface ReporterProps {
    reporter: Reporter
}

export interface Age {
    id: string
    title: ReactElement<any, any>
}

export interface IntlProps {
    intl: IntlShape
}
