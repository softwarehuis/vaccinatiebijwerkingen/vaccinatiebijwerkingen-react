/**
 Copyright (C) 2021 Softwarehuis

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Form, Input, Radio } from "antd"
import { Component } from "react"
import { FormattedMessage, IntlShape } from "react-intl"
import { expert, IntlProps, Reporter } from "../Core"

const whoOptions = [
    {
        label: <FormattedMessage
            id="who.self"
            description="Myself"
            defaultMessage="I report for myself" />, value: 0
    },
    {
        label: <FormattedMessage
            id="who.others"
            description="Others"
            defaultMessage="I report for others" />, value: 1
    },
]

const professionalOptions = [
    {
        label: <FormattedMessage
            id="professional.no"
            description="I have no medical background"
            defaultMessage="No" />, value: false
    },
    {
        label: <FormattedMessage
            id="professional.yes"
            description="I am a medical professional"
            defaultMessage="Yes" />, value: true
    },
]

interface Form1Props {
    intl: IntlShape
    reporter: Reporter
    onChangeWho: any
    onChangeProfessional: any
    onChangeProfession: any
    onChangeExpert: any
}

export class Form1 extends Component<Form1Props, IntlProps> {
    render() {
        const { reporter, onChangeWho, onChangeProfessional, onChangeProfession, onChangeExpert, intl } = this.props
        return (<Form initialValues={this.props.reporter} layout="vertical">
            <Form.Item
                name="who"
                label={intl.formatMessage({ id: "who", defaultMessage: "I want to report for" })}
                extra={intl.formatMessage({ id: "question.who", defaultMessage: "For who are you reporting?" })}
            >
                <Radio.Group
                    options={whoOptions}
                    onChange={onChangeWho}
                    value={reporter.who}
                    optionType="button"
                />
            </Form.Item>
            <Form.Item
                name="professional"
                label={intl.formatMessage({ id: "professional", defaultMessage: "Do you have a medical background?" })}
                extra={intl.formatMessage({ id: "question.professional", defaultMessage: "Are you a medical professional, a doctor, general practitioner or otherwise?" })}
            >
                <Radio.Group
                    options={professionalOptions}
                    onChange={onChangeProfessional}
                    value={reporter.professional}
                    optionType="button"
                />
            </Form.Item>
            {reporter.professional &&
                <><Form.Item
                    name="profession"
                    label={intl.formatMessage({ id: "profession", defaultMessage: "Profession" })}
                >
                    <Input
                        value={reporter.profession}
                        onChange={onChangeProfession}
                        placeholder={intl.formatMessage({ id: "question.profession", defaultMessage: "What profession?" })}
                    />
                </Form.Item>
                <Form.Item
                name="expert"
                label={intl.formatMessage({ id: "expert", defaultMessage: "Expert" })}
                extra={intl.formatMessage({ id: "question.expert", defaultMessage: "Can you provide formal medical descriptions?" })}
            >
                <Radio.Group
                    options={expert}
                    onChange={onChangeExpert}
                    value={reporter.expert}
                    optionType="button"
                />
            </Form.Item></>
            }
        </Form>
        )
    }
}