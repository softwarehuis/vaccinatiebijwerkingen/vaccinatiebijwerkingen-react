/**
 Copyright (C) 2021 Softwarehuis

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Form, Input, Radio, Select } from "antd"
import { Component } from "react"
import { IntlShape } from "react-intl";
import { Reporter, Report, deceased, autopsy, reasons, ages, medical } from "../Core";
//import { municipalitiesToCascader } from "../data/types";

const { Option } = Select

interface Form3Props {
    reporter: Reporter
    onChangeReport: any
    intl: IntlShape
}

// function areaFilter(inputValue: any, path: any) {
//     return path.some((option: any) => option.label.toLowerCase().indexOf(inputValue.toLowerCase()) > -1);
// }

export class Form3 extends Component<Form3Props> {
    constructor(props: Form3Props) {
        super(props)
        this.onChangeReason = this.onChangeReason.bind(this);
    }

    readonly state: { currentReport: Report } = {
        currentReport: {
            key: -1,
            name: "",
            area: "",
            deceased: false,
            autopsy: false,
            medical: false,
            age: "<20",
            reasons: []
        }
    }

    onChangeName = (e: any) => {
        // This function requires the onChangeReport to be wrapped in the callback
        this.setState({
            currentReport: {
                ...this.state.currentReport,
                name: e.target.value
            },
        }, () => {
            this.props.onChangeReport(
                this.state.currentReport, (key: number) => {
                    this.setState({
                        currentReport: {
                            ...this.state.currentReport,
                            key: key
                        }
                    })
                }
            )
        })
    }

    onChangeArea = (e: any) => {
        // This function requires the onChangeReport to be wrapped in the callback
        this.setState({
            currentReport: {
                ...this.state.currentReport,
                area: e.target.value
            },
        }, () => {
            this.props.onChangeReport(
                this.state.currentReport, (key: number) => {
                    this.setState({
                        currentReport: {
                            ...this.state.currentReport,
                            key: key
                        }
                    })
                }
            )
        })
    }

    onChangeAge = (value: string) => {
        // This function requires the onChangeReport to be wrapped in the callback
        this.setState({
            currentReport: {
                ...this.state.currentReport,
                age: value
            },
        }, () => {
            this.props.onChangeReport(
                this.state.currentReport, (key: number) => {
                    this.setState({
                        currentReport: {
                            ...this.state.currentReport,
                            key: key
                        }
                    })
                }
            )
        })
    }

    onChangeReason(value: any) {
        // This function needs binding, see constructor
        this.setState({
            currentReport: {
                ...this.state.currentReport,
                reasons: value
            },
        }, () => {
            this.props.onChangeReport(
                this.state.currentReport, (key: number) => {
                    this.setState({
                        currentReport: {
                            ...this.state.currentReport,
                            key: key
                        }
                    })
                }
            )
        })
    }

    onChangeDeceased = (e: any) => {
        // This function requires the onChangeReport to be wrapped in the callback
        this.setState({
            currentReport: {
                ...this.state.currentReport,
                deceased: e.target.value,
            },
        }, () => {
            this.props.onChangeReport(
                this.state.currentReport, (key: number) => {
                    this.setState({
                        currentReport: {
                            ...this.state.currentReport,
                            key: key
                        }
                    })
                }
            )
        })
    }

    onChangeMedical = (e: any) => {
        // This function requires the onChangeReport to be wrapped in the callback
        this.setState({
            currentReport: {
                ...this.state.currentReport,
                medical: e.target.value
            },
        }, () => {
            this.props.onChangeReport(
                this.state.currentReport, (key: number) => {
                    this.setState({
                        currentReport: {
                            ...this.state.currentReport,
                            key: key
                        }
                    })
                }
            )
        })
    }

    onChangeAutopsy = (e: any) => {
        // This function requires the onChangeReport to be wrapped in the callback
        this.setState({
            currentReport: {
                ...this.state.currentReport,
                autopsy: e.target.value
            },
        }, () => {
            this.props.onChangeReport(
                this.state.currentReport, (key: number) => {
                    this.setState({
                        currentReport: {
                            ...this.state.currentReport,
                            key: key
                        }
                    })
                }
            )
        })
    }

    render() {
        const { reporter, intl } = this.props
        return (<Form initialValues={this.state.currentReport} layout="vertical">
            {this.state.currentReport.key > -1 && reporter.who === 1 && 
            <span>Report {this.state.currentReport.key + 1}</span>
            }
            <Form.Item label={intl.formatMessage({ id: "name", defaultMessage: "Name" })} name="name" rules={[{ required: true }]}>
                <Input onChange={this.onChangeName} placeholder={reporter.who === 0 ?
                    intl.formatMessage({ id: "question.name_self", defaultMessage: "What is your name?" })
                    :
                    intl.formatMessage({
                        id: "question.name_other",
                        defaultMessage: "What is the name of the person you report for?"
                    })
                } />
            </Form.Item>


            <Form.Item label={intl.formatMessage({ id: "area", defaultMessage: "Area" })} name="area" rules={[{ required: true }]}>
                <Input onChange={this.onChangeArea} placeholder={reporter.who === 0 ?
                    intl.formatMessage({ id: "question.area_self", defaultMessage: "In what area do you live?" })
                    :
                    intl.formatMessage({
                        id: "question.area_other",
                        defaultMessage: "In what area does/did this person live?"
                    })
                } />
            </Form.Item>

            <Form.Item name="age" label={intl.formatMessage({ id: "age", defaultMessage: "Age" })} rules={[{ required: true }]}>
                <Select onChange={this.onChangeAge}>
                    {ages.map(age => {
                        return (
                            <Option key={age.id} value={age.id}>
                                {age.title}
                            </Option>
                        )
                    })}
                </Select>
            </Form.Item>
            <Form.Item name="reasons" label={intl.formatMessage({ id: "reason", defaultMessage: "Reason" })}>
                <Select
                    mode="tags"
                    allowClear
                    placeholder={intl.formatMessage({ id: "question.reason_choose", defaultMessage: "What was the reason for vaccination?" })}
                    onChange={this.onChangeReason}
                >
                    {reasons}
                </Select>
            </Form.Item>
            {reporter.who === 1 &&
                <Form.Item
                    name="deceased"
                    label={intl.formatMessage({ id: "deceased", defaultMessage: "Deceased" })}
                    extra={intl.formatMessage({ id: "question.deceased", defaultMessage: "Is the person you report for deceased?" })}
                >
                    <Radio.Group
                        options={deceased}
                        onChange={this.onChangeDeceased}
                        value={this.state.currentReport.deceased}
                        optionType="button"
                    />
                </Form.Item>
            }
            {this.state.currentReport.deceased &&
                <Form.Item
                    name="autopsy"
                    label={intl.formatMessage({ id: "autopsy", defaultMessage: "Autopsy" })}
                    extra={intl.formatMessage({ id: "question.autopsy", defaultMessage: "Has autopsy been committed?" })}
                >
                    <Radio.Group
                        options={autopsy}
                        onChange={this.onChangeAutopsy}
                        value={this.state.currentReport.autopsy}
                        optionType="button"
                    />
                </Form.Item>
            }
            <Form.Item
                name="medical"
                label={intl.formatMessage({ id: "medical", defaultMessage: "Report" })}
                extra={intl.formatMessage({ id: "question.medical", defaultMessage: "Do you have a official medical report?" })}
            >
                <Radio.Group
                    options={medical}
                    onChange={this.onChangeMedical}
                    value={this.state.currentReport.medical}
                    optionType="button"
                />
            </Form.Item>

        </Form>
        )
    }
}