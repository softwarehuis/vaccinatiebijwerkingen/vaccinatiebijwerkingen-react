/**
 Copyright (C) 2021 Softwarehuis

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Form, Input } from "antd"
import { Component } from "react"
import { IntlShape } from "react-intl";
import { Reporter } from "../Core";

interface Form2Props {
    reporter: Reporter
    onFinish: any
    intl: IntlShape
}

export class Form2 extends Component<Form2Props> {
    readonly state: { userInfo: any } = {
        userInfo: {
            name: "",
            telephone: "",
            email: ""
        }
    }

    onChangeName = (e: any) => {
        this.setState({
            currentReport: {
                ...this.state.userInfo,
                name: e.target.value
            },
        })
    }

    onChangeTelephone = (e: any) => {
        this.setState({
            currentReport: {
                ...this.state.userInfo,
                telephone: e.target.value
            },
        })
    }

    onChangeEmail = (e: any) => {
        // This function requires the onChangeReport to be wrapped in the callback
        this.setState({
            currentReport: {
                ...this.state.userInfo,
                email: e.target.value
            },
        })
    }

    render() {
        const { intl } = this.props
        return (<Form id="VerificationForm" initialValues={this.state.userInfo} layout="vertical" onFinish={this.props.onFinish}>
            <Form.Item label={intl.formatMessage({ id: "name", defaultMessage: "Name" })} name="name" rules={[{ required: true }]}>
                <Input onChange={this.onChangeName} placeholder={
                    intl.formatMessage({ id: "question.name_self", defaultMessage: "What is your name?" })
                } />
            </Form.Item>
            <Form.Item label={intl.formatMessage({ id: "telephone", defaultMessage: "Mobile" })} name="telephone" rules={[{ required: true }]}>
                <Input onChange={this.onChangeTelephone} placeholder={
                    intl.formatMessage({ id: "question.telephone", defaultMessage: "What is your  mobile number?" })
                } />
            </Form.Item>
            <Form.Item name="email" label={intl.formatMessage({ id: "email", defaultMessage: "Email" })} rules={[{ required: true }]}>
                <Input onChange={this.onChangeTelephone} placeholder={
                    intl.formatMessage({ id: "question.email", defaultMessage: "What is your email address?" })
                } />
            </Form.Item>
        </Form>
        )
    }
}