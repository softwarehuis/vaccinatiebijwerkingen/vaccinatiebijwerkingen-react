# Getting Started with vaccinatiebijwerkingen-react

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Install
Make sure you have [**node**](https://nodejs.org/en/download/). We use [yarn](https://yarnpkg.com/getting-started/install) as package manager.

To install all dependencies or update them, run:
### `yarn`

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Localization

Please do not add regular strings to the source code, use the `<FormattedMessage />` directive. Once your code is done, run the script:

### `yarn extract 'src/**/*.tsx' --out-file lang/en.json --id-interpolation-pattern '[sha512:contenthash:base64:6]'`

So the English translation source gets updated. Then run:

### `yarn compile lang/en.json --ast --out-file src/lang/en.json`

So the English compiled language file gets (re)created. You can then tell translators it is time to update their source files.


## Learn More

To learn React, check out the [React documentation](https://reactjs.org/).
